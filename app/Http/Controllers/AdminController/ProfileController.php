<?php

namespace App\Http\Controllers\AdminController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Profile;
use App\EmailGeneratePdf;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['active'] = 'profile';
        $data['profiles'] = Profile::orderBy('sorting', 'asc')->get();
        return view('admin/profile', $data);
    }

    public function create(Request $request){

        $profile = new Profile();

        if($request->hasFile('image_before') && $request->hasFile('image_after')){
            $ext = $request->file('image_before')->getClientOriginalExtension();
            $filename1 = rand(10,10001).time().'.'.$ext;

            $upload = $request->file('image_before')->storeAs(
                'public/uploads', $filename1
            );
            $profile->image_before = $filename1;

            $ext = $request->file('image_after')->getClientOriginalExtension();
            $filename2 = rand(10,10001).time().'.'.$ext;

            $upload = $request->file('image_after')->storeAs(
                'public/uploads', $filename2
            );
            $profile->image_after = $filename2;
        }
        $profile->title_it = $request->input('title_it');
        $profile->title_en = $request->input('title_en');
        $profile->title_de = $request->input('title_de');
        $profile->title_py = $request->input('title_py');

        $profile->description_it = $request->input('description_it');
        $profile->description_en = $request->input('description_en');
        $profile->description_de = $request->input('description_de');
        $profile->description_py = $request->input('description_py');

        $profile->tipologia_it = $request->input('tipologia_it');
        $profile->tipologia_en = $request->input('tipologia_en');
        $profile->tipologia_de = $request->input('tipologia_de');
        $profile->tipologia_py = $request->input('tipologia_py');

        $profile->price_it = $request->input('price_it');
        $profile->price_en = $request->input('price_en');
        $profile->price_de = $request->input('price_de');
        $profile->price_py = $request->input('price_py');

        $profile->location_it = $request->input('location_it');
        $profile->location_en = $request->input('location_en');
        $profile->location_de = $request->input('location_de');
        $profile->location_py = $request->input('location_py');

        $profile->mq_it = $request->input('mq_it');
        $profile->mq_en = $request->input('mq_en');
        $profile->mq_de = $request->input('mq_de');
        $profile->mq_py = $request->input('mq_py');
        
        $profile->network_it = $request->input('network_it');
        $profile->network_en = $request->input('network_en');
        $profile->network_de = $request->input('network_de');
        $profile->network_py = $request->input('network_py');
        $profile->sorting = $request->input('sorting');
        $profile->save();

        return redirect()->route('profile.show')->with('status', 'Profile Created!');

    }

    public function showEditForm($id)
    {
        $data['active'] = 'profile';
        $data['profile'] = Profile::where('id', $id)->first();
        return view('admin/edit_profile', $data);
    }

    public function update(Request $request){
        $id = $request->input('id');
        $profile = Profile::where('id', $id)->first();
        if($request->hasFile('image_before')){
            if($profile->image_before){
                Storage::delete('public/uploads/'.$profile->image_before);
            }
            $ext = $request->file('image_before')->getClientOriginalExtension();
            $filename = rand(10,120000).time().'.'.$ext;

            $upload = $request->file('image_before')->storeAs(
                'public/uploads', $filename
            );
            $profile->image_before = $filename;

        }

        if($request->hasFile('image_after')){
            if($profile->image_after){
                Storage::delete('public/uploads/'.$profile->image_after);
            }

            $ext = $request->file('image_after')->getClientOriginalExtension();
            $filename = rand(10,12000).time().'.'.$ext;

            $upload = $request->file('image_after')->storeAs(
                'public/uploads', $filename
            );
            $profile->image_after = $filename;
        }

        $profile->title_it = $request->input('title_it');
        $profile->title_en = $request->input('title_en');
        $profile->title_de = $request->input('title_de');
        $profile->title_py = $request->input('title_py');

        $profile->description_it = $request->input('description_it');
        $profile->description_en = $request->input('description_en');
        $profile->description_de = $request->input('description_de');
        $profile->description_py = $request->input('description_py');

        $profile->tipologia_it = $request->input('tipologia_it');
        $profile->tipologia_en = $request->input('tipologia_en');
        $profile->tipologia_de = $request->input('tipologia_de');
        $profile->tipologia_py = $request->input('tipologia_py');

        $profile->price_it = $request->input('price_it');
        $profile->price_en = $request->input('price_en');
        $profile->price_de = $request->input('price_de');
        $profile->price_py = $request->input('price_py');

        $profile->location_it = $request->input('location_it');
        $profile->location_en = $request->input('location_en');
        $profile->location_de = $request->input('location_de');
        $profile->location_py = $request->input('location_py');

        $profile->mq_it = $request->input('mq_it');
        $profile->mq_en = $request->input('mq_en');
        $profile->mq_de = $request->input('mq_de');
        $profile->mq_py = $request->input('mq_py');
        
        $profile->network_it = $request->input('network_it');
        $profile->network_en = $request->input('network_en');
        $profile->network_de = $request->input('network_de');
        $profile->network_py = $request->input('network_py');
        $profile->sorting = $request->input('sorting');
        $profile->save();

        return redirect()->route('profile.show')->with('status', 'Profile Updated!');
    }

    public function delete($id) {
        $profile = Profile::where('id', $id)->first();
        $profile->delete();
        return redirect()->route('profile.show')->with('status', 'Profile Deleted!');
    }


    public function getEmailGeneratedPdf(){
        $data['active'] = 'il-tuo-architetto';
        $data['emailGeneratePdfs'] = EmailGeneratePdf::orderBy('id', 'desc')->get();
        return view('admin/emailGeneratedPdf', $data);
    }

}
