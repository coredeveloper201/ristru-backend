<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('profiles', 'ProfileController@all');
Route::get('projects', 'ProfileController@projects');
Route::get('profile/{title}/{id}', 'ProfileController@get');
Route::post('send-mail', 'ProfileController@postSendEmail');
Route::get('generated-pdf/{id}', 'ProfileController@getGeneratedPdf');
