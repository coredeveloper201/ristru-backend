<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_it');
            $table->string('title_en');
            $table->string('title_de');
            $table->string('title_py');
            $table->text('description_it')->nullable();
            $table->text('description_en')->nullable();
            $table->text('description_de')->nullable();
            $table->text('description_py')->nullable();
            $table->string('image_before')->nullable();
            $table->string('image_after')->nullable();
            $table->string('tipologia_it')->nullable();
            $table->string('tipologia_en')->nullable();
            $table->string('tipologia_de')->nullable();
            $table->string('tipologia_py')->nullable();
            $table->string('price_it')->nullable();
            $table->string('price_en')->nullable();
            $table->string('price_de')->nullable();
            $table->string('price_py')->nullable();
            $table->string('location_it')->nullable();
            $table->string('location_en')->nullable();
            $table->string('location_de')->nullable();
            $table->string('location_py')->nullable();
            $table->string('network_it')->nullable();
            $table->string('network_en')->nullable();
            $table->string('network_de')->nullable();
            $table->string('network_py')->nullable();
            $table->string('mq_it')->nullable();
            $table->string('mq_en')->nullable();
            $table->string('mq_de')->nullable();
            $table->string('mq_py')->nullable();
            $table->string('network_it')->nullable();
            $table->string('network_en')->nullable();
            $table->string('network_de')->nullable();
            $table->string('network_py')->nullable();
            $table->integer('sorting')->default(999)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
