
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Ristrutturazione Case</title>

        <link rel="stylesheet" href="https://dev2.simonechinaglia.net/dev/ristru/css/bootstrap.css">
        <link rel="stylesheet" href="https://dev2.simonechinaglia.net/dev/ristru/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://dev2.simonechinaglia.net/dev/ristru/css/owl.theme.default.css">
        <link rel="stylesheet" href="https://dev2.simonechinaglia.net/dev/ristru/css/owl.carousel.css">
        <!--<link rel="stylesheet" href="https://dev2.simonechinaglia.net/dev/ristru/fonts/stylesheet.css">-->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
        <link rel="stylesheet" href="https://dev2.simonechinaglia.net/dev/ristru/css/main.css">
        <link rel="stylesheet" href="https://dev2.simonechinaglia.net/dev/ristru/css/overlap.css">
        
    </head>
    <body>
        <div id="app"></div>

        
        
        <script src="https://dev2.simonechinaglia.net/dev/ristru/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="https://dev2.simonechinaglia.net/dev/ristru/js/vendor/bootstrap.js"></script>
        <script src="https://dev2.simonechinaglia.net/dev/ristru/js/owl.carousel.js"></script>
        <script src="{{ asset('/js/app.js') }}" type="text/javascript"></script>
    </body>
</html>