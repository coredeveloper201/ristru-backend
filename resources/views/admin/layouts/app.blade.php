<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Ristru</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('public/plug/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('public/plug/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/style/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/style/app.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/style/style.css') }}">
    <!-- <link href="{{ asset('public/css/app.css') }}" rel="stylesheet"> -->

    @stack('style')
</head>
<body>
    <div id="app">
        <section id="top-navbar" class="nav4">
            @include('admin.layouts.navbar')
        </section>

        <div id="layout">
            <div id="sidebar">
                @include('admin.layouts.sidebar')
            </div>
            <div id="content">
                @yield('content')
            </div>
        </div>
       
    </div>

    <!-- Scripts -->
    <script src="{{ asset('public/plug/jquery.js')}}"></script>
    <script src="{{ asset('public/plug/tether.js')}}"></script>
    <script src="{{ asset('public/plug/bootstrap.js')}}"></script>
    <script src="{{ asset('public/plug/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/js/dist/js/select2.min.js')}}"></script>
    <script>
    $('.select2').select2();
    </script>
    <script type="text/javascript">
        $('.datatables').DataTable();
    </script>
    
    {{-- <script src="{{ asset('public/js/app.js') }}"></script> --}}
    <script src="{{ asset('public/js/dist/js/app.js')}}"></script>

    @stack('js')
</body>
</html>
