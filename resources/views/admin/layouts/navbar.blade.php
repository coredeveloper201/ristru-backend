<div class="nav fixed-top">
    <!-- navbar left -->
    <div class="nav-left text-center">
        <a href="{{ route('profile.show') }}" class="brand"><img height="40" src="{{ asset('public/img/logo.png') }}" alt=""></a>
    </div>
    <!-- navbar right -->
    <div class="nav-right">
        <div class="left">
            <a id="NavBtn" href="#" class="btn btn-link">
            <i class="zmdi zmdi-menu"></i>
            </a>
        </div>
        <div class="right">
            <div class="user dropdown">
                
                
                    <a href="{{ route('logout') }}" class="btn"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        <i class="zmdi zmdi-input-power"></i> Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                
            </div>  
        </div>
    </div>
</div>
