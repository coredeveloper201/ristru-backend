@extends('admin.layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-head">
                <div class="left">IL TUO ARCHITETTO</div>
                <div class="right">
                <i class="zmdi zmdi-chevron-down"></i>
                <i class="zmdi zmdi-refresh-sync"></i>
                <i class="zmdi zmdi-close-circle-o"></i>
                </div>
            </div>
            <div class="card-body">
                <table class="datatables display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nome</th>
                            <th>Cellulare</th>
                            <th>Email</th>
                            <th>Room</th>
                            <th>Data <br>Dell'incontro</th>
                            <th>Architetto <br>Incaricato</th>
                            <th>Città</th>
                            <th>File</th>
                            <th width="80"><i class="zmdi zmdi-settings"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(sizeof($emailGeneratePdfs))
                            @foreach($emailGeneratePdfs as $pdf)
                                <tr>
                                    <td>{{ $pdf->id }}</td>
                                    <td>{{ $pdf->name }}</td>
                                    <td>{{ $pdf->phone }}</td>
                                    <td>{{ $pdf->email }}</td>
                                    <td>{{ $pdf->room }}</td>
                                    <td>{{ ($pdf->date) ? date("d-m-Y", strtotime($pdf->date)): '-' }}</td>
                                    <td>{{ ($pdf->want_architect == true) ? 'Yes': 'No' }}</td>
                                    <td>{{ $pdf->flag }}</td>
                                    @if($pdf->file)
                                        <td><a href="{{ asset('public/storage/uploads/'.$pdf->file) }}" target="_blank">{{ $pdf->file }}</a></td>
                                    @else 
                                        <td>-</td>
                                    @endif
                                    <td><a href="https://dev2.simonechinaglia.net/pdf/{{ $pdf->id }}" class="btn btn-sm btn-info text-center" target="_blank"><i class="zmdi zmdi-eye"></i></a></td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
