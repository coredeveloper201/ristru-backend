import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(Vuex);
Vue.use(VueAxios, axios);

// Vue.use(require('vue-resource'));

export const store = new Vuex.Store({
    state : {
        lang : 'it',
        auth : {},
        profiles: []
    },
    getters: {
        profiles(state) {
            return state.profiles;
        }
	},
	mutations: {
		updateProfiles(state, payload) {
            state.profiles = payload;
        }
	},
	actions: {
        getProfiles(context) {
            console.log("Calling");
            axios.get('/api/profiles')
            .then((response) => {
                context.commit('updateProfiles', response.data.profiles);
            })
        }
	}
});

