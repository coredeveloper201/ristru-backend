import Home from '../components/home.vue'
import Architect from '../components/architect.vue'
import SingleArchitect from '../components/single_architect.vue'
import Ambienti from '../components/ambienti.vue'
import Portfolio from '../components/portfolio.vue'
import SinglePortfolio from '../components/single_portfolio.vue'
import Contact from '../components/contact.vue'
import PageSingle from '../components/page_single.vue'
import FromStep from '../components/form_step.vue'
import Pdf from '../components/pdf.vue'

export const routes = [
    {path : '/' , component : Home},
    {path : '/architect' , component : Architect},
    {path : '/architect/:name' , component : SingleArchitect},
    {path : '/ambienti' , component : Ambienti},
    {path : '/progetti' , component : Portfolio},
    {path : '/progetti/:title/:id' , component : SinglePortfolio},
    {path : '/contact' , component : Contact},
    {path : '/page-single' , component : PageSingle},
    {path : '/il-tuo-architetto' , component : FromStep},
    {path : '/pdf' , component : Pdf},
];